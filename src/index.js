if (process.env.NODE_ENV !== "production") {
  console.log("Development mode");
}

/* Bootstrap’s JavaScript */
import "bootstrap";

/* Component script */
import "./components/heading/heading";
import "./components/noscript/noscript";

/* Components styles */
import "./components/styles.scss";
